# Design Patterns

The Patterns implemented here as examples were explained in the course "Software Engineering" at the Bern University of Applied Sciences.

## Structure

The patterns are split into the types suggested by the the GoF (Gang of Four: Erich Gamma, Richard Helm, Ralph Johnson and John Vlissides):
- creational patterns
- structural patterns
- behavioral patterns

Each Pattern has its own package. 
The name of the package is the name of the pattern which it contains.

## Links

Useful links with further explanations:
- [DigitalOcean on GoF patterns](https://www.digitalocean.com/community/tutorials/gangs-of-four-gof-design-patterns)
- [Springframework-Guru on GoF patterns](https://springframework.guru/gang-of-four-design-patterns/)

## Disclaimer

The implementations may contain errors and be not correct according to the theoretical specification.