package ch.haeberli.structural.adapter;

public class IntegerPrinter implements IntegerConsumer {

    @Override
    public int consume(Integer i) {
        System.out.println(i);
        return i;
    }
}
