package ch.haeberli.structural.adapter;

public class IntegerMultiplicator implements IntegerConsumer {

    private Integer factor;

    public IntegerMultiplicator(Integer factor) {
        this.factor = factor;
    }

    @Override
    public int consume(Integer i) {
        return i * factor;
    }
}
