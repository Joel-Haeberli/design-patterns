package ch.haeberli.structural.adapter;

public class StringToIntegerAdapter<T extends IntegerConsumer> {

    int adapt(String s, IntegerConsumer consumer) {

        try {
            Integer fromString = Integer.valueOf(s);
            return consumer.consume(fromString);
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("String is not a number!");
        }
    }
}
