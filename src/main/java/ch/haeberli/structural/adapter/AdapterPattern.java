package ch.haeberli.structural.adapter;

/**
 * The adapter pattern adapts inputs to an underlying implementation of a functionality by knowing how to translate the
 * input that the underlying implementation can be used. An adapter maps requests to a different interface which does
 * then the job by delegation of the adapter. The request and response will come from the adapter.
 */
public class AdapterPattern {

    public static void main(String[] args) {

        StringToIntegerAdapter<IntegerConsumer> adapter = new StringToIntegerAdapter<>();

        int first = adapter.adapt("2", new IntegerPrinter());
        int second = adapter.adapt("2", new IntegerMultiplicator(3));
        System.out.println(first);
        System.out.println(second);
    }
}
