package ch.haeberli.structural.adapter;

public interface IntegerConsumer {

    int consume(Integer i);
}
