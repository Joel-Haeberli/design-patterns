package ch.haeberli.structural.composite;

/**
 * The composite pattern describes a way to tackle similar or even identical tasks for different object-types.
 * The composite is the interface to these operations. In the example, there are the operations 'print' and 'doThings'.
 * 'print' just prints out something defined by the respective composable. 'doThings' just applies some changes to all
 * composables.
 */
public class CompositePattern {

    public static void main(String[] args) {

        Composite composite = new Composite();

        composite.add(new ComposableA());
        composite.add(new ComposableB());

        composite.print();
        composite.doThings();

        composite.add(new ComposableC());

        composite.print();
        composite.doThings();

        composite.print();
    }
}
