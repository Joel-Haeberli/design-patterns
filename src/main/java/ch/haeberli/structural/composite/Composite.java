package ch.haeberli.structural.composite;

import java.util.ArrayList;
import java.util.List;

public class Composite {

    private final List<Composable> composables = new ArrayList<>();

    void print() {
        composables.forEach(Composable::print);
    }

    void doThings() {
        composables.forEach(Composable::doThings);
    }

    void add(Composable composable) {
        composables.add(composable);
    }
}
