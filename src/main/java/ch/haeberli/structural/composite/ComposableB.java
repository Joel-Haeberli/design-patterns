package ch.haeberli.structural.composite;

public class ComposableB implements Composable {

    Integer i = 0;

    @Override
    public void print() {
        System.out.println("Counter composable: " + i);
    }

    @Override
    public void doThings() {
        i++;
    }
}
