package ch.haeberli.structural.composite;

public interface Composable {

    void print();
    void doThings();
}
