package ch.haeberli.structural.composite;

public class ComposableC implements Composable {

    boolean state = false;

    @Override
    public void print() {
        System.out.println("Boolean Composable: " + state);
    }

    @Override
    public void doThings() {
        state = !state;
    }
}
