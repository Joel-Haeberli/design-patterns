package ch.haeberli.structural.composite;

public class ComposableA implements Composable {

    private String str = "A";

    @Override
    public void print() {
        System.out.println("String concatenator: " + str);
    }

    @Override
    public void doThings() {
        str += "B";
    }
}
