package ch.haeberli.behavioral.decorator;

public abstract class AbstractName {

    protected String name = "Unknown Base Object";

    public abstract String getName();
}
