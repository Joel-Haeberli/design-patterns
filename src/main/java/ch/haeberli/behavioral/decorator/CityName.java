package ch.haeberli.behavioral.decorator;

public class CityName extends NameDecorator {

    public CityName(AbstractName injectedName) {
        this.name = injectedName.getName();
    }

    @Override
    public String getName() {
        return "The name of the city is " + super.getName();
    }
}
