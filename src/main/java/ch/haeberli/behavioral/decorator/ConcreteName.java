package ch.haeberli.behavioral.decorator;

public class ConcreteName extends AbstractName {

    public ConcreteName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
