package ch.haeberli.behavioral.decorator;

public class PeopleName extends NameDecorator {

    public PeopleName(AbstractName injectedName) {
        this.name = injectedName.getName();
    }

    @Override
    public String getName() {
        return "The name of the person is " + super.getName();
    }
}
