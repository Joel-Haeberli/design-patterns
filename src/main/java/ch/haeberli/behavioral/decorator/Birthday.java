package ch.haeberli.behavioral.decorator;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Birthday extends NameDecorator {

    private LocalDate birthday;

    public Birthday(AbstractName injectedName, LocalDate birthday) {
        this.name = injectedName.getName();
        this.birthday = birthday;
    }

    @Override
    public String getName() {
        return super.getName() + ", and has birthday on the " + DateTimeFormatter.ofPattern("dd. MMMM (yyyy)").format(birthday);
    }
}
