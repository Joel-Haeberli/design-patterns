package ch.haeberli.behavioral.decorator;

import java.time.LocalDate;

/**
 * Modify Behaviour of a certain class at runtime.
 * <p>
 * Be compliant with the Open-Closed Principle.
 *    Be open to extensions but closed to modifications
 */
public class DecoratorPattern {

    public static void main(String[] args) {
        AbstractName person = new Birthday(new PeopleName(new ConcreteName("Joel")), LocalDate.of(1999, 11, 13));
        AbstractName city = new CityName(new ConcreteName("Bern"));

        System.out.println(person.getName());
        System.out.println(city.getName());
    }
}
