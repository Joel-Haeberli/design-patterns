package ch.haeberli.behavioral.decorator;

public abstract class NameDecorator extends AbstractName {

    @Override
    public String getName() {
        return name;
    }
}
