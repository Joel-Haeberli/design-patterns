package ch.haeberli.behavioral.command;

@FunctionalInterface
public interface Command<T> {
    T execute();
}
