package ch.haeberli.behavioral.command;

public class HelloCommand implements Command<String> {

    @Override
    public String execute() {
        return "Hello!";
    }
}
