package ch.haeberli.behavioral.command;

public class Invoker {

    Command[] cmds = {new HelloCommand(), new HelloCommand(), new ByeCommand()};

    public void invokeCommands() {
        for (Command cmd : cmds) {
            System.out.println("Command Result: " + cmd.execute());
        }
    }
}
