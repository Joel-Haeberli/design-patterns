package ch.haeberli.behavioral.command;

public class CommandPattern {

    public static void main(String[] args) {

        Invoker invoker = new Invoker();

        invoker.invokeCommands();
    }
}
