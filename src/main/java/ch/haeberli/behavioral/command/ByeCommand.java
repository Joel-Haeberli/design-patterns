package ch.haeberli.behavioral.command;

public class ByeCommand implements Command<String> {


    @Override
    public String execute() {
        return "Bye!";
    }
}
