package ch.haeberli.behavioral.strategy;

public class StrategyA implements Strategy<StrategyEnforcer.StrategyParameters> {

    @Override
    public void fulfill(StrategyEnforcer.StrategyParameters params) {
        System.out.println(params.somerandomfieldB + " / " + params.somerandomfieldA);
    }
}
