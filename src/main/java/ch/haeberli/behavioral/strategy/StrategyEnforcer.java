package ch.haeberli.behavioral.strategy;

/**
 * The Strategy enforcer is the object in the system which depends on the different strategy to fulfill its tasks.
 * It executes the strategies received within its tasks.
 * <p>
 * All what is does is delegating the part of the tasks which must be fulfilled by a certain strategy to the strategy chosen.
 * Like this the same aim can be reached in different ways.
 * <p>
 * In this example, the strategies simply print the string and the integer of the parameters in an inverse way from each other.
 */
public class StrategyEnforcer<P> {

    void applyStrategy(Strategy<P> strategy, P parameters) {
        strategy.fulfill(parameters);
    }

    public static class StrategyParameters {
        String somerandomfieldA;
        Integer somerandomfieldB;

        public StrategyParameters(String somerandomfieldA, Integer somerandomfieldB) {
            this.somerandomfieldA = somerandomfieldA;
            this.somerandomfieldB = somerandomfieldB;
        }
    }
}
