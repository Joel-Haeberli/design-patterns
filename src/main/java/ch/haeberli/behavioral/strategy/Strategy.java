package ch.haeberli.behavioral.strategy;

/**
 * This is an abstract strategy which is used as interface to an arbitrary strategy.
 */
public interface Strategy<I> {
    void fulfill(I input);
}
