package ch.haeberli.behavioral.strategy;

public class StrategyB implements Strategy<StrategyEnforcer.StrategyParameters> {

    @Override
    public void fulfill(StrategyEnforcer.StrategyParameters params) {
        System.out.println(params.somerandomfieldA + " / " + params.somerandomfieldB);
    }
}
