package ch.haeberli.behavioral.strategy;

public class StrategyPattern {

    public static void main(String[] args) {

        StrategyEnforcer<StrategyEnforcer.StrategyParameters> executor = new StrategyEnforcer<>();

        Strategy<StrategyEnforcer.StrategyParameters> strategyA = new StrategyA();
        Strategy<StrategyEnforcer.StrategyParameters> strategyB = new StrategyB();

        executor.applyStrategy(strategyA, new StrategyEnforcer.StrategyParameters("Hi!", 100));
        executor.applyStrategy(strategyB, new StrategyEnforcer.StrategyParameters("Hi!", 100));
    }
}
