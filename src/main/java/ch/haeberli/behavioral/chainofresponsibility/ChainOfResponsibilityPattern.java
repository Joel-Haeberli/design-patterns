package ch.haeberli.behavioral.chainofresponsibility;

public class ChainOfResponsibilityPattern {

    public static void main(String[] args) {

        Number n = Double.valueOf("23.5");
        Number addAndMultiply = Double.valueOf("34.32");
        new PrintProcessor<>(
                new AddProcessor(
                        new MultiplyProcessor(
                                new PrintProcessor<>(null), addAndMultiply), addAndMultiply))
                .process(n);
    }
}
