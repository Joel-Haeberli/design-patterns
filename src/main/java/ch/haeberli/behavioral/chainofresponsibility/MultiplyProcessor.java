package ch.haeberli.behavioral.chainofresponsibility;

public class MultiplyProcessor extends Processor<Number> {

    private final Number factor;

    public MultiplyProcessor(Processor<Number> next, Number factor) {
        super(next);
        this.factor = factor;
    }

    @Override
    void internalProcess(Number request) {
        Number result = request.doubleValue() * factor.doubleValue();
        new PrintProcessor<Number>(null).process(result);
    }
}
