package ch.haeberli.behavioral.chainofresponsibility;

public class PrintProcessor<R> extends Processor<R> {

    public PrintProcessor(Processor<R> next) {
        super(next);
    }

    @Override
    void internalProcess(R request) {
        System.out.println("Processing Request: " + request);
    }
}
