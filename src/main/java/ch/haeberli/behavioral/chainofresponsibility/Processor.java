package ch.haeberli.behavioral.chainofresponsibility;

public abstract class Processor<R> {

    private final Processor<R> next;

    public Processor(Processor<R> next) {
        this.next = next;
    }

    void process(R request) {
        internalProcess(request);
        if (next != null)
            next.process(request);
    }

    abstract void internalProcess(R request);
}
