package ch.haeberli.behavioral.chainofresponsibility;

public interface Handler<R> {
    void handle(R request);
}
