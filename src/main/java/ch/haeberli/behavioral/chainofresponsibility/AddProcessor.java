package ch.haeberli.behavioral.chainofresponsibility;

public class AddProcessor extends Processor<Number> {

    private final Number toAdd;

    public AddProcessor(Processor<Number> next, Number toAdd) {
        super(next);
        this.toAdd = toAdd;
    }

    @Override
    void internalProcess(Number request) {
        Number newValue = request.doubleValue() + toAdd.doubleValue();
        new PrintProcessor<Number>(null).process(newValue);
    }
}
