package ch.haeberli.behavioral.iterator;

public class IteratorPattern {

    public static void main(String[] args) {

        Integer[] iterable = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};

        Iterator<Integer> iterator = new Iterator<>(iterable);

        System.out.println("Iterator Entries:");
        while (iterator.hasNext()) {
            System.out.println("Element: " + iterator.next());
        }
    }
}
