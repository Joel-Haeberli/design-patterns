package ch.haeberli.behavioral.iterator;

public class Iterator<T> {

    private final T[] iterable;
    private int pos = -1;

    public Iterator(T[] iterable) {
        this.iterable = iterable;
    }

    public T next() {
        pos++;
        return iterable[pos];
    }

    public boolean hasNext() {
        return pos < iterable.length - 1;
    }
}
