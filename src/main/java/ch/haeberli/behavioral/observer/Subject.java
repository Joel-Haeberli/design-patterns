package ch.haeberli.behavioral.observer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A subject is an object which emits new events.
 * Observers may register to a subject and catch these events if they are interested in them.
 */
public class Subject {

    Set<Observer> observers = new HashSet<>();

    public void register(Observer o) {
        observers.add(o);
    }

    public void emit(Event e) {
        observers.forEach(o -> o.consume(e));
    }
}
