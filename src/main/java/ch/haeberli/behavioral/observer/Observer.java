package ch.haeberli.behavioral.observer;

import java.util.Random;

/**
 * An observer observes subjects which he is interested in and acts accordingly.
 */
public class Observer {

    private final Integer id = new Random().nextInt(0, 100);

    public void consume(Event e) {
        System.out.println("Got event, observer-id: " + id);
    }
}
