package ch.haeberli.behavioral.observer;

/**
 * The Events are the messages sent in the system.
 * A subject emits them to its observers.
 */
public interface Event {
}
