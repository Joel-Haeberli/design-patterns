package ch.haeberli.behavioral.observer;

public class ObserverPattern {

    public static void main(String[] args) {

        Subject s = new Subject();

        Observer firstObserver = new Observer();
        Observer secondObserver = new Observer();

        s.register(firstObserver);
        s.register(secondObserver);
        s.emit(new Event() {});

        Observer thirdObserver = new Observer();

        s.register(thirdObserver);
        s.emit(new Event() {});
    }
}
