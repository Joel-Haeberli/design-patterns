package ch.haeberli.creational.abstractfactory;

public class AbstractFactoryPattern {

    public static void main(String[] args) {

        AbstractFactory<ThingA, String> factoryA = new FactoryA();
        AbstractFactory<ThingB, Integer> factoryB = new FactoryB();

        ThingA a1 = factoryA.create("First object");
        ThingA a2 = factoryA.create("second object");
        ThingB b1 = factoryB.create(1);
        ThingB b2 = factoryB.create(2);

        System.out.println(a1.i);
        System.out.println(a2.i);
        System.out.println(b1.i);
        System.out.println(b2.i);
    }
}
