package ch.haeberli.creational.abstractfactory;

public class ThingB {

    Integer i;

    public ThingB(Integer i) {
        this.i = i;
    }
}
