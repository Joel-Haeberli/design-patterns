package ch.haeberli.creational.abstractfactory;

public class FactoryB implements AbstractFactory<ThingB, Integer> {

    @Override
    public ThingB create(Integer input) {
        return new ThingB(input);
    }
}
