package ch.haeberli.creational.abstractfactory;

/**
 * The Abstract Factory Pattern describes an interface to create a factory of a given type.
 * In my example I'm using generics. You can also make a specific abstract factory with a specific type.
 * @param <R>
 */
interface AbstractFactory<R, I> {

    /**
     * Creates an object of the type R with input I.
     *
     * @param input The Input to create the Object of type R.
     * @return an object of type R
     */
    R create(I input);
}
