package ch.haeberli.creational.abstractfactory;

public class FactoryA implements AbstractFactory<ThingA, String> {

    @Override
    public ThingA create(String input) {
        return new ThingA(input);
    }
}
