package ch.haeberli.creational.abstractfactory;

/**
 * Marker-Interface
 * Each class implementing this interface is capable of being created by the factory.
 */
public interface Factorizable {
}
