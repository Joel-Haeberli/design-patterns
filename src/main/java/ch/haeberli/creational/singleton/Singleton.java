package ch.haeberli.creational.singleton;

/**
 * The singleton pattern creates only one instance of
 * a class. The instantiation is encapsulated in a public method.
 * The constructor is private and called from this method.
 */
public class Singleton {

    private static Singleton instance;

    private Singleton() {}

    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }

    public static void main(String[] args) {
        Singleton getFirst = Singleton.getInstance();
        Singleton getSecond = Singleton.getInstance();
        assert getFirst == getSecond;
    }
}
