package ch.haeberli.creational.builder;

public class BuilderPattern {

    public static void main(String[] args) {
        Dummy d = new Dummy.DummyBuilder()
                .id(2)
                .str("I was built")
                .build();

        Dummy d2 = new Dummy.DummyBuilder()
                .id(1983)
                .str("I was built too")
                .build();

        System.out.println("Dummy : " + d);
        System.out.println("   id : " + d.getId());
        System.out.println("   str: " + d.getStr());

        System.out.println("Dummy : " + d2);
        System.out.println("   id : " + d2.getId());
        System.out.println("   str: " + d2.getStr());
    }
}
