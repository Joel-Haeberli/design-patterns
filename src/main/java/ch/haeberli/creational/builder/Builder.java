package ch.haeberli.creational.builder;

public interface Builder<T> {

    T build();
}
