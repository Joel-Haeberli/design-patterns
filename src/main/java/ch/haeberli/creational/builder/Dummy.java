package ch.haeberli.creational.builder;

public class Dummy {

    private final int id;
    private final String str;

    private Dummy(int id, String str) {
        this.id = id;
        this.str = str;
    }

    public int getId() {
        return id;
    }

    public String getStr() {
        return str;
    }

    static class DummyBuilder implements Builder<Dummy> {

        private int id;
        private String str;

        public DummyBuilder id(int id) {
            this.id = id;
            return this;
        }

        public DummyBuilder str(String str) {
            this.str = str;
            return this;
        }

        @Override
        public Dummy build() {
            return new Dummy(id, str);
        }
    }
}
