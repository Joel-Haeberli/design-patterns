package ch.haeberli.creational.factory;

import java.util.Random;

/**
 * Class to be instantiated
 */
class ToBeCreatedOther implements Factorizable {

    public Integer randomInteger = new Random().nextInt();
}
