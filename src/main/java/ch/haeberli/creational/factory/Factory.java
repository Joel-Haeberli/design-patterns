package ch.haeberli.creational.factory;

/**
 * The normal factory has the capability of building objects of a given Typ.
 * In our case it is an Interface. Like this we can capsule the creation of
 * the Type to be any of subtypes of the interfaces.
 */
public class Factory {

    public enum Type { ONE, THE_OTHER }

    public Factorizable create(Type t) {
        switch (t) {
            case ONE -> {
                return new ToBeCreated();
            }
            case THE_OTHER -> {
                return new ToBeCreatedOther();
            }
        }
        throw new IllegalStateException("unknown type");
    }
}
