package ch.haeberli.creational.factory;

public class FactoryPattern {

    public static void main(String[] args) {
        Factory f = new Factory();
        Factorizable one = f.create(Factory.Type.ONE);
        Factorizable theOther = f.create(Factory.Type.THE_OTHER);

        System.out.println(one);
        System.out.println(theOther);
    }
}
