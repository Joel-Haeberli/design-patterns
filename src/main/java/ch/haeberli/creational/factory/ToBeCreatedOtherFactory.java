package ch.haeberli.creational.factory;

/**
 * Specific Factory
 */
public class ToBeCreatedOtherFactory extends Factory {

    public Factorizable createObject() {
        return new ToBeCreatedOther();
    }
}
