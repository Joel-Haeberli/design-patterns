package ch.haeberli.creational.factory;

/**
 * Specific Factory
 */
public class ToBeCreatedFactory extends Factory {

    public Factorizable createObject() {
        return new ToBeCreated();
    }
}
