package ch.haeberli.creational.factory;

/**
 * Class to be instantiated
 */
public class ToBeCreated implements Factorizable {

    public String randomvalue = "Random string.";
}
